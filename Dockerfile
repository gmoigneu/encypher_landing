FROM nginx:stable-alpine
COPY ./www.conf /etc/nginx/conf.d/default.conf
COPY . /var/www/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]